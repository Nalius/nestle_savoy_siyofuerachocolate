  var respuestas = new Array();
  $(document).on('click','.crear',function(){
    von();
      $.get('./structure/content.php', {num:2}, function(render) {
          $('.container-fluid').html(render);
          voff();
      });
  });
  $(document).on('click','#goRealiza',function(){
    if($('.chocoactivo').size() > 0){
          von();
      $.get('./structure/content.php', {num:3}, function(render) {
        var clase = $('.chocoactivo').parent('.imgContainer').parent('.circular').attr('class').split(' ')[1];
        switch (clase) {
          case 'leche':
              setValue(0,clase);
          break;
          case 'blanco':
              setValue(0,clase);
          break;
          case 'negro':
              setValue(0,clase);
          break;
        }
          $('.container-fluid').html(render);
          voff();
      });
    }else{
        $('.velo').fadeIn();
    }
  });
  $(document).on('click','#goMezcla',function(){
      if($('.chocoactivo').size() > 0){
            von();
        $.get('./structure/content.php', {num:4}, function(render) {
          var clase = $('.chocoactivo').parent('.imgContainer').parent('.circular').attr('class').split(' ')[1];
          switch (clase) {
            case 'merey':
                setValue(1,clase);
            break;
            case 'naranjas':
                setValue(1,clase);
            break;
            case 'pistacho':
                setValue(1,clase);
            break;
            case 'cafe':
                setValue(1,clase);
            break;
            case 'fresas':
                setValue(1,clase);
            break;
            case 'pasas':
                setValue(1,clase);
            break;
            case 'solo':
                setValue(1,clase);
            break;
          }
            $('.container-fluid').html(render);
            voff();
        });
      }else{
        $('.velo').fadeIn();
      }
  });
  $(document).on('click','#goEscoge',function(){
    if($('.focus').size() > 0){
      von();
      $.get('./structure/content.php', {num:5}, function(render) {
        /*var clase = $('.focus').attr('class').split(' ')[1];
        switch (clase) {
          case 'quince':
              setValue(2,clase);
          break;
          case 'treinta':
              setValue(2,clase);
          break;
          case 'cuatrocinco':
              setValue(2,clase);
          break;
        }*/
        $('.container-fluid').html(render);
        voff();
      })
    }else{
        $('.velo').fadeIn();
    }
  });
  $(document).on('click','#goResultados',function(){
      if($('.focus').size() > 0){
        $('.terminado').fadeIn();
        //resultados();
      }else{
          $('.velo').fadeIn();
      }
  });
  $(document).on('click','#volverEscoge',function(){
    von();
      $.get('./structure/content.php', {num:4}, function(render) {
          $('.container-fluid').html(render);
          voff();
      });
  });
  $(document).on('click','#volverMezcla',function(){
    von();
      $.get('./structure/content.php', {num:3}, function(render) {
          $('.container-fluid').html(render);
          voff();
      });
  });
  $(document).on('click','#volverRealiza',function(){
    von();
      $.get('./structure/content.php', {num:2}, function(render) {
          $('.container-fluid').html(render);
          voff();
      });
  });
  $(document).on('click','.btnVolverEmpezar',function(){
    von();
      $.get('./structure/content.php', {num:1}, function(render) {
          $('.container-fluid').html(render);
          voff();
      });
  });
  $(document).on('click','.circular',function(){
    $('.bordes').hide();
    $('.circular .imgContainer').children('img').removeClass('chocoactivo');
    $(this).children('.bordes').show();
    $(this).children('.imgContainer').children('img').addClass('chocoactivo');
  });
  $(document).on('click','.timeCircle',function(){
    $('.timeCircle').removeClass('focus');
    $(this).addClass('focus');
  });
  $(document).on('click','.size',function(){
    $('.size .sizeimg').removeClass('focus');
    $(this).children('.sizeimg').addClass('focus');
  });
  $(document).on('click','.velo',function(){
    $(this).fadeOut();
  });
  $(document).on('click','#cerrarEsteVelo',function(){
      $('.terminado').fadeOut();
  });
  $(document).on('click','.verResultado',function(){
      resultados();
  });
  $(document).on('click','#compartir',function(){
      loginFacebook();
  });
//FUNCTIONS
  function setValue(pos,value){
      respuestas[pos] = value;
  }
  function resultados(){
    von();
    $.get('./structure/content.php', {num:6}, function(render) {
        $('.container-fluid').html(render)
        var a = muestra();
        var titulo = 'titulo-'+a[0]+'-'+a[1];
        var texto;
        switch (titulo) {
            case 'titulo-chb-r1':
              texto= 'Tus frases favoritas son “mi pana”, “hermano del alma” y “compadre”. Para ti, cualquier esquina es buena para conocer a un extraño que en 2 horas, se convertirá en tu “mejor amigo”. Eres capaz de inventar cualquier cosa con tal de sacarle conversación a alguien, porque eso de estar solito y sin tema de conversación, ¡no es lo tuyo!';
            break;
            case 'titulo-chb-r2':
              texto= 'Eres de los que ayudan sin esperar nada a cambio. En todos lados das los “buenos días”, pides con “por favor” y das las “gracias” y es que eres de esos que ayudan a la doñitas a cruzar la calle, los que detienen el ascensor y sostienen la puerta… ¡Tú eres tremendo personaje! Porque ser atento y detallista, sin duda es tu mayor virtud.';
            break;
            case 'titulo-chb-r3':
              texto= 'El dicho “Soñar no cuesta nada” es tu lema de vida. Cuando te acuestas, pasas horas y horas pensando en tus metas y nuevos proyectos. Sacas lo máximo de cada experiencia y cada error es un aprendizaje para el próximo reto. Soñar despierto es tu día a día, porque cuando te fijas un objetivo, ¡no descansas hasta alcanzarlo!';
            break;
            case 'titulo-chb-r4':
              texto= 'Eres ese pana con ganas incansables de parrandear y que siempre tiene un chiste para todo. Te la pasas alegrando a todo el mundo y cuando dejas de hacerlo, todos te empiezan a extrañar jaja. Y es que tú eres de esos que no puede estar mucho tiempo sin soltar una carcajada porque  eso de caras largas “nada que ver”. ';
            break;
            case 'titulo-chb-r5':
              texto= 'Eres de esos venezolanos sencillos, con buen sentido del humor, de trato fácil y conversador… De los que  sabe convivir en armonía y evita a toda costa cualquier tipo de discusión, porque tu único propósito, es pasarla ¡lo más chévere posible!';
            break;
            case 'titulo-chb-r6':
              texto= 'Tu originalidad te ha convertido en ese venezolano jocoso que asimila las “malas noticias” con mucha, mucha creatividad. Eres de lo que busca darle la vuelta a todo, viéndole el lado positivo a las cosas y convirtiendo lo negativo… ¡en un motivo para hacer sonreír a los demás! ';
            break;
            case 'titulo-chb-r7':
              texto= 'No te paras en artículo para decir lo que piensas. Eres de los que habla por el grupo, en el metro o en la calle, sin pensarlo dos veces. Muchos no saben de lo que eres capaz de lograr, hasta que los dejas boca abiertos. Tu grito de guerra siempre será “¡Yo mismo soy!” ';
            break;



            case 'titulo-chdl-r1':
              texto= 'Eres de los que ayudan sin esperar nada a cambio. En todos lados das los “buenos días”, pides con “por favor” y das las “gracias” y es que eres de esos que ayudan a la doñitas a cruzar la calle, los que detienen el ascensor y sostienen la puerta… ¡Tú eres tremendo personaje! Porque ser atento y detallista, sin duda es tu mayor virtud.';
            break;
            case 'titulo-chdl-r2':
              texto= 'El dicho “Soñar no cuesta nada” es tu lema de vida. Cuando te acuestas, pasas horas y horas pensando en tus metas y nuevos proyectos. Sacas lo máximo de cada experiencia y cada error es un aprendizaje para el próximo reto. Soñar despierto es tu día a día, porque cuando te fijas un objetivo, ¡no descansas hasta alcanzarlo!';
            break;
            case 'titulo-chdl-r3':
              texto= 'Eres ese pana con ganas incansables de parrandear y que siempre tiene un chiste para todo. Te la pasas alegrando a todo el mundo y cuando dejas de hacerlo, todos te empiezan a extrañar jaja. Y es que tú eres de esos que no puede estar mucho tiempo sin soltar una carcajada porque  eso de caras largas “nada que ver”. ';
            break;
            case 'titulo-chdl-r4':
              texto= 'Tus frases favoritas son “mi pana”, “hermano del alma” y “compadre”. Para ti, cualquier esquina es buena para conocer a un extraño que en 2 horas, se convertirá en tu “mejor amigo”. Eres capaz de inventar cualquier cosa con tal de sacarle conversación a alguien, porque eso de estar solito y sin tema de conversación, ¡no es lo tuyo! ';
            break;
            case 'titulo-chdl-r5':
              texto= 'La algarabía y la pasión te acompañan pa’ donde quiera que vayas, pero al final del camino eres pura tradición venezolana, de arepa, dominó, momentos en familia y trabajo duro. Tú eres, la digna representación de lo más venezolano… que tenemos los venezolanos.  ';
            break;
            case 'titulo-chdl-r6':
              texto= 'Eres de esos que se para tempranito a trabajar con ganas de comerse el mundo. No importa qué tan fea se ponga la cosa, tú siempre estás de buen ánimo porque tu familia está ahí para apoyarte  y tú para sacarlos adelante.';
            break;
            case 'titulo-chdl-r7':
              texto= 'Eres de esos que le hace la “segunda” a cualquiera, el que se cala los guayabos, el que le presta a otro pana cuando está pelando y el que responde: “dale” a cualquier locura. Te pueden contar el chisme o el secreto  más cabilla del mundo… pero eres ese pana fiel en el que se puede confiar. ';
            break;


            case 'titulo-cho-r1':
              texto= 'Eres de esos que le hace la “segunda” a cualquiera, el que se cala los guayabos, el que le presta a otro pana cuando está pelando y el que responde: “dale” a cualquier locura. Te pueden contar el chisme o el secreto  más cabilla del mundo… pero eres ese pana fiel en el que se puede confiar. ';
            break;
            case 'titulo-cho-r2':
              texto= 'Eres de esos que se para tempranito a trabajar con ganas de comerse el mundo. No importa qué tan fea se ponga la cosa, tú siempre estás de buen ánimo porque tu familia está ahí para apoyarte  y tú para sacarlos adelante.';
            break;
            case 'titulo-cho-r3':
              texto= 'Tu originalidad te ha convertido en ese venezolano jocoso que asimila las “malas noticias” con mucha, mucha creatividad. Eres de lo que busca darle la vuelta a todo, viéndole el lado positivo a las cosas y convirtiendo lo negativo… ¡en un motivo para hacer sonreír a los demás!';
            break;
            case 'titulo-cho-r4':
              texto= 'No te paras en artículo para decir lo que piensas. Eres de los que habla por el grupo, en el metro o en la calle, sin pensarlo dos veces. Muchos no saben de lo que eres capaz de lograr, hasta que los dejas boca abiertos. Tu grito de guerra siempre será “¡Yo mismo soy!” ';
            break;
            case 'titulo-cho-r5':
              texto= 'Pa’ donde vayas, tú siempre andas pelando el diente y contagiando la buena vibra con Raimundo y todo el mundo. Puedes pasar horas y horas hablando pistoladas sin aburrirte, y es que sonreír, es de venezolanos buena gente.';
            break;
            case 'titulo-cho-r6':
              texto= 'Pa’ donde vayas, tú siempre andas pelando el diente y contagiando la buena vibra con Raimundo y todo el mundo. Puedes pasar horas y horas hablando pistoladas sin aburrirte, y es que sonreír, es de venezolanos buena gente.';
            break;
            case 'titulo-cho-r7':
              texto= 'Eres de esos venezolanos sencillos, con buen sentido del humor, de trato fácil y conversador… De los que  sabe convivir en armonía y evita a toda costa cualquier tipo de discusión, porque tu único propósito, es pasarla ¡lo más chévere posible!';
            break;
        }
        $('.topCopy').addClass('titulo-'+a[0]+'-'+a[1]);
        $('.resultadoImagen').addClass(''+a[0]+'-'+a[1]+'');
        $('.resultadoDescripcion').children('p').html(texto);
        voff();
    });
  }
  function muestra(){
    var retorno = new Array();
    var tipoChocolate,saborChocolate,titulo;
    for (var i = 0; i < respuestas.length; i++) {
      switch (respuestas[0]) {
        case 'leche':
            tipoChocolate = 'chdl';
            retorno[0]=tipoChocolate;
        break;
        case 'blanco':
            tipoChocolate = 'chb';
            retorno[0]=tipoChocolate;
        break;
        case 'negro':
            tipoChocolate = 'cho';
            retorno[0]=tipoChocolate;
        break;
      }
      switch (respuestas[1]) {
        case 'merey':
            saborChocolate= 'r2';
            retorno[1]=saborChocolate;
        break;
        case 'naranjas':
            saborChocolate= 'r1';
            retorno[1]=saborChocolate;
        break;
        case 'pistacho':
            saborChocolate= 'r6';
            retorno[1]=saborChocolate;
        break;
        case 'cafe':
            saborChocolate= 'r7';
            retorno[1]=saborChocolate;
        break;
        case 'fresas':
            saborChocolate= 'r3';
            retorno[1]=saborChocolate;
        break;
        case 'pasas':
            saborChocolate= 'r4';
            retorno[1]=saborChocolate;
        break;
        case 'solo':
            saborChocolate= 'r5';
            retorno[1]=saborChocolate;
        break;
      }
      /*switch (respuestas[2]) {
        case 'quince':

        break;
        case 'treinta':

        break;
        case 'cuatrocinco':

        break;
      }
      switch (respuestas[3]) {
        case 'pequeno':

        break;
        case 'vertical':

        break;
        case 'mediano':

        break;
      }*/
        return retorno;
    }
  }
  function shareFacebook(){
    var url =  $('.resultadoImagen').css('background-image').replace('url','');
    url = url.replace('(','');
    url = url.replace(')','');
    url = url.replace('"','');
    url = url.replace('"','');
    console.log(url);
     FB.ui({
        method: 'feed',
        link: 'https://hacemosloquenosgusta.com/siyofuerachocolate/tab.php',
        description: $('.resultadoDescripcion p').html() ,
        picture: url,
        caption: 'SI YO FUERA CHOCOLATE',
        }, function(response){
            console.log(response);
        });
}

  function loginFacebook(){
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
  }
  function statusChangeCallback(response) {
  	if (response.status == 'connected') {
  		FB.api('/me',{fields:'first_name,last_name,email'},function(user){
  	 		shareFacebook();
  		});
  	} else if (response.status == 'not_authorized') {
  		FB.login(function(login){
  			FB.api('/me',{fields:'first_name,last_name,email'},function(user){
  	 				shareFacebook();
  			});
  		},{scope:'email,public_profile,user_photos,publish_actions'});
  	}else{
  		FB.login(function(login){
  			FB.api('/me',{fields:'first_name,last_name,email'},function(user){
  	 			shareFacebook();
  			});
  		},{scope:'email,public_profile'});
  	}
  }
  function von(){
     $(window).scrollTop(0);
    $('.velo_general').fadeIn();
  }
  function voff(){
    $('.velo_general').fadeOut();
  }
