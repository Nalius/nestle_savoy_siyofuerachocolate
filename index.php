<?php
	if(!$_SERVER['HTTPS']== 'on'){
		$url_https="https://". $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		header("Location: $url_https");
		exit();
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<?php include_once('structure/head_content.php'); ?>
	</head>
	<body>
        <div class="velo_general" style="position: absolute;margin: 0 auto;width: 810px;background: rgba(0,0,0,0.7);z-index: 999;left: 0;right: 0;height: 860px;display:none;">
	        <img style="position:absolute; top:30vh; width:250px; left:0; right:0; margin:0 auto 0 auto;"  src="images/loader.gif" alt="loader" />
	    </div>
		<div class="wrapper">
	      <div class="container-fluid">
		      <?php include_once('structure/content.php'); ?>
      	</div>
	  </div>
	</body>
</html>
