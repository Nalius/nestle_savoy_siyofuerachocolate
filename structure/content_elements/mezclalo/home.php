<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
	<div class="col-xs-12 col-sm-12 col-md-12 mod_ppal">
		<div class="velo">
			<img src="images/yavachamo.png" alt="" />
		</div>
		<div class="mezclaContainer">
			<div class="topCopy">

			</div>
			<div class="chocoContainer">
					<p>Bátelo a tu gusto, el tiempo que desees, para darle
					la consistencia que más te gusta a tu chocolate.</p>
						<div class="timeCircles">
							<div class="timeHolder">

								<div class="timeCircle quince">
									<div class="timeCircleimg">

									</div>
									<p>Súper espeso</p>
								</div>
								<div class="timeCircle treinta">
									<div class="timeCircleimg">

									</div>
									<p>Como el de siempre</p>
								</div>
								<div class="timeCircle cuatrocinco">
									<div class="timeCircleimg">

									</div>
									<p>Bien cremosito</p>
								</div>
						</div>
						</div>
						<div class="flechas">

						</div>
					</div>
					<div class="siguiente realizaSiguiente">
						<button id="volverMezcla" class="btnVolver" type="button" name="button"></button>
						<button id="goEscoge" class="btnSiguiente" type="button" name="button"></button>
					</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$('.slider').slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
	});
</script>
