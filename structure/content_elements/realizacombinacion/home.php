<div class="col-xs-12 col-sm-12 col-md-12 mod_toston">
	<div class="col-xs-12 col-sm-12 col-md-12 mod_ppal">
		<div class="velo">
			<img src="images/elpaisa.png" alt="" />
		</div>
		<div class="realizaContainer">
			<div class="topCopy">

			</div>
			<div class="chocoContainer">
					<p>Dale un sabor único a tu chocolate agregándole lo que más te guste.</p>
					<div class="realizaHolder">
					<div class="circles slider">
							<div class="circular merey">
									<div class="bordes"></div>
										<div class="imgContainer">
											<img src="images/merey.png" alt="leche" />
										</div>
									<p>Sabor venezolano <br>
									a merey</p>
							</div>
							<div class="circular naranjas">
								<div class="bordes"></div>
									<div class="imgContainer">
										<img src="images/narajas.png" alt="blanco" />
									</div>
								<p>Lo cítrico<br>
								de las naranjitas</p>
							</div>
							<div class="circular pistacho">
								<div class="bordes"></div>
									<div class="imgContainer">
										<img src="images/pistacho.png" alt="negro" />
									</div>
								<p>Lo exótico<br>
								del pistacho</p>
							</div>
							<div class="circular cafe">
									<div class="bordes"></div>
										<div class="imgContainer">
											<img src="images/cafe.png" alt="leche" />
										</div>
									<p>Sabor fuerte<br>
									a café</p>
							</div>
							<div class="circular fresas">
								<div class="bordes"></div>
									<div class="imgContainer">
										<img src="images/fresas.png" alt="blanco" />
									</div>
								<p>Lo acidito<br>
								de las fresas</p>
							</div>
							<div class="circular pasas">
								<div class="bordes"></div>
									<div class="imgContainer">
										<img src="images/pasas.png" alt="negro" />
									</div>
								<p>La suavidad<br>
								de las pasas</p>
							</div>
							<div class="circular solo">
								<div class="bordes"></div>
									<div class="imgContainer">
										<img src="images/solo.png" alt="negro" />
									</div>
								<p>Puro sabor<br>
								a chocolate Savoy®</p>
							</div>
					</div>
					</div>
					<div class="flechas">

					</div>
			</div>
			<div class="siguiente realizaSiguiente">
				<button id="volverRealiza" class="btnVolver" type="button" name="button"></button>
				<button id="goMezcla" class="btnSiguiente" type="button" name="button"></button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
if($(window).width() < 767){
	$('.circles').removeClass("slider");
}
	$('.slider').slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
	});
</script>
