<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="expires" content="0">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SAVOY®</title>
<!-- Jquery -->
<script src="js/lib/jquery-1.11.3.min.js"></script>
<script src="js/lib/jquery-migrate-1.2.1.min.js"></script>
<!-- Bootstrap -->
<script src="js/lib/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<script src="js/lib/jquery_ui.js"></script>
<script src="js/lib/jquery.ui.touch-punch.min.js"></script>
<!-- http://kenwheeler.github.io/slick/ -->
<script src="js/lib/slick.min.js"></script>
<!-- Style Sheet -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/styles_chocolate.css" rel="stylesheet">
<link href="css/slick-1.5.7/slick.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/slick-1.5.7/slick-theme.css">
<link rel="stylesheet" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css" media="screen" title="no title" charset="utf-8">
<link rel="stylesheet" href="css/jquery_ui.css">

<!-- Owl Carousel Assets >
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet"-->
